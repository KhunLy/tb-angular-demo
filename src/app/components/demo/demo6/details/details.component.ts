import { Component, OnInit, Input } from '@angular/core';
import { Film } from '../../../../models/film';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  
  @Input() film:Film;

  constructor() { }

  ngOnInit(): void {
  }

}
