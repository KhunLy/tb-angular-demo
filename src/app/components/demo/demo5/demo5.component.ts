import { Component, OnInit } from '@angular/core';
import { Product } from '../../../models/Product';

@Component({
  selector: 'app-demo5',
  templateUrl: './demo5.component.html',
  styleUrls: ['./demo5.component.scss']
})
export class Demo5Component implements OnInit {

  list: Product[]; 

  constructor() { }

  ngOnInit(): void {
    this.list = [
      { expirationDate: new Date(2020,7,1), name: "Coca Cola", price: 1.2, image: "coca.jpg" },
      { expirationDate: new Date(2020,8,1), name: "Fanta", price: 1.3, image: "fanta.jpg" },
      { expirationDate: new Date(2020,9,1), name: "Dr Pepper", price: 1.5, image: "pepper.jpg" },
    ];
  }

}
