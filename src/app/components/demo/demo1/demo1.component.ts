import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {

  public maVariable : string;
  maVariable1 : number;
  maVariable2 : Date;
  
  constructor() { }

  ngOnInit(): void {
    this.maVariable = 'Valeur de ma Variable';
    this.maVariable1 = 42.42222222;
    this.maVariable2 = new Date();
    setTimeout(() => {
      this.maVariable1 = 75;
    } , 3000);
  }

}
