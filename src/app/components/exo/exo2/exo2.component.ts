import { Component, OnInit } from '@angular/core';
import { Article } from '../../../models/article';
import { NbDialogService } from '@nebular/theme';
import { ConfirmBoxComponent } from '../../confirm-box/confirm-box.component';

@Component({
  selector: 'app-exo2',
  templateUrl: './exo2.component.html',
  styleUrls: ['./exo2.component.scss']
})
export class Exo2Component implements OnInit {

  list: Article[];

  userEntry: string;

  constructor(
    private dialogService: NbDialogService
  ) { }

  ngOnInit(): void {
    this.list = [];
  }

  add() {
    let foundedItem = this.list.find(a => a.name.toUpperCase().trim() == this.userEntry.toUpperCase().trim());
    if(foundedItem){
      //increase
      foundedItem.quantity++;
    }
    else{
      this.list.push({ name: this.userEntry, isChecked : false, quantity : 1 });
    }
    this.userEntry = null;
  }

  delete(article: Article) {
    // let i = this.list.indexOf(article);
    // this.list.splice(i, 1);
    this.dialogService.open(ConfirmBoxComponent)
      .onClose.subscribe(data => {
        if(data){
          this.list = this.list.filter(a => a != article);
        }
      });
  }
  
  check(article: Article) {
    article.isChecked = !article.isChecked;
  }
}
