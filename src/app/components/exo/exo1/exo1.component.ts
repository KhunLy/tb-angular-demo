import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exo1',
  templateUrl: './exo1.component.html',
  styleUrls: ['./exo1.component.scss']
})
export class Exo1Component implements OnInit {

  compt: number;
  timer: any;

  constructor() { }

  ngOnInit(): void {
    this.compt = 0;
  }

  start() {
    this.timer = setInterval(() => {
      this.compt++;
    }, 10);
  }

  stop() {
    clearInterval(this.timer);
    this.timer = null;
  }

  reset() {
    this.stop();
    this.compt = 0;
  }
}
