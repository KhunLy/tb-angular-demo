import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { RouterModule } from '@angular/router';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { DemoComponent } from './components/demo/demo.component';
import { Demo1Component } from './components/demo/demo1/demo1.component';
import { Demo2Component } from './components/demo/demo2/demo2.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbButtonModule, NbMenuModule, NbIconModule, NbSidebarModule, NbCardModule, NbInputModule, NbSelectModule, NbListModule, NbDialogModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { ExoComponent } from './components/exo/exo.component';
import { Exo1Component } from './components/exo/exo1/exo1.component';
import { ToTimePipe } from './pipes/to-time.pipe';
import { Demo3Component } from './components/demo/demo3/demo3.component';
import { Demo4Component } from './components/demo/demo4/demo4.component';
import { FormsModule } from '@angular/forms';
import { Exo2Component } from './components/exo/exo2/exo2.component';
import { Demo5Component } from './components/demo/demo5/demo5.component';
import { ImagesFolderPipe } from './pipes/images-folder.pipe';
import { ExpiredPipe } from './pipes/expired.pipe';
import { ConfirmBoxComponent } from './components/confirm-box/confirm-box.component';
import { Demo6Component } from './components/demo/demo6/demo6.component';
import { HttpClientModule } from '@angular/common/http';
import { DetailsComponent } from './components/demo/demo6/details/details.component';
import { Demo6DetailsComponent } from './components/demo/demo6-details/demo6-details.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    AboutComponent,
    DemoComponent,
    Demo1Component,
    Demo2Component,
    ExoComponent,
    Exo1Component,
    ToTimePipe,
    Demo3Component,
    Demo4Component,
    Exo2Component,
    Demo5Component,
    ImagesFolderPipe,
    ExpiredPipe,
    ConfirmBoxComponent,
    Demo6Component,
    DetailsComponent,
    Demo6DetailsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'about', component: AboutComponent },
      { path: 'demo', component: DemoComponent, children: [
        { path: 'demo1', component: Demo1Component },
        { path: 'demo2', component: Demo2Component },
        { path: 'demo3', component: Demo3Component },
        { path: 'demo4', component: Demo4Component },
        { path: 'demo5', component: Demo5Component },
        { path: 'demo6', component: Demo6Component },
        { path: 'demo6-details/:id', component: Demo6DetailsComponent },
      ] },
      { path: 'exo', component: ExoComponent, children: [
        { path: 'exo1', component: Exo1Component },
        { path: 'exo2', component: Exo2Component },
      ] }
    ]),
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbMenuModule.forRoot(),
    NbIconModule,
    NbSidebarModule.forRoot(),
    NbCardModule,
    NbInputModule,
    NbSelectModule,
    NbListModule,
    //nécessessaire pour le binding 2 ways
    FormsModule,
    NbDialogModule.forRoot(),
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
