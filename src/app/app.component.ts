import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  links: NbMenuItem[];
  ngOnInit() {
    this.links = [
      { link: '/home', title: 'Home', icon: 'home' },
      { link: '/about', title: 'About', icon: 'star' },
      { title: 'Demo', icon: 'book', children: [
        { link: '/demo/demo1', title: 'Demo1 - Binding' },
        { link: '/demo/demo2', title: 'Demo2 - Event' },
        { link: '/demo/demo3', title: 'Demo3 - Structural directives' },
        { link: '/demo/demo4', title: 'Demo4 - Binding 2 ways' },
        { link: '/demo/demo5', title: 'Demo5 - Models' },
        { link: '/demo/demo6', title: 'Demo6 - HttpClient' },
      ] }, 
      { title: 'Exercices', icon: 'bookmark' , children: [
        { link : '/exo/exo1', title: 'Exercice 1 - Timer' },
        { link : '/exo/exo2', title: 'Exercice 2 - Shopping list' },
      ] }
    ]
  }
}
